# CL_Deronne_TP1

Maxime DERONNE Génie Logiciel

## Comment lancer l'application

En étant dans la racine du projet:

        javac -cp ini4j-0.5.4.jar src/main/java/cl_deronne_tp1/ConfigReader.java

Ensuite :

        java -cp src/main/java:ini4j-0.5.4.jar cl_deronne_tp1/ConfigReader

ou :

        java -cp src/main/java:ini4j-0.5.4.jar cl_deronne_tp1/ConfigReader --configuration <chemin_du_fichier>