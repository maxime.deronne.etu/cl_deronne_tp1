package cl_deronne_tp1;

//import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ConfigReader {

    private static final String DEFAULT_PATH = "./file.ini";

    public static void main(String[] args) {
        try{
            int index = 0;
            for(int i = 0; i< args.length; i++){
                if(args[i].equals("--configuration")){
                    index = i+1;
                }
            }
            Map<String,String> env = System.getenv();
            String envPath = env.get("FICHIER_DE_CONFIGURATION");
            File file;
            if(index != 0){
                String filePath = args[index];
               file = new File(filePath);
            } else if (envPath != null) {
                file = new File(envPath);
            }
            else {
                file = new File(DEFAULT_PATH);
            }
            System.out.println(new Wini(file).toString());
        } catch (NullPointerException e){
            System.out.println("Le fichier n'existe pas.");
 //       } catch (InvalidFileFormatException e) {
 //           throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
